<?php
    require_once dirname(__FILE__) . '/../../config/DbManager.php';
    $pdo = getDb();

    $shop = $_POST["shop_id"];
    $left = $_POST["left2right"];
    $right = $_POST["right2left"];
    $created = $_POST["created_at"];
    $updated = $_POST["updated_at"];

    if(isset($shop) && isset($left) && isset($right)){

        $sql = "INSERT INTO dev_shop_status (id, shop_id, left2right, right2left, left_in, created_at, updated_at) SELECT :id, :shop, :left, :right, left_in, :createdAt, :updatedAt FROM dev_shop WHERE shop_id = :shop";

        $stmt = $pdo -> prepare($sql);
        $params = array(':id' => NULL, ':shop' => $shop, ':left' => $left, ':right' => $right, ':createdAt' => $created, ':updatedAt' => $updated);

        $flag = $stmt -> execute($params);
        if($flag){
        //    print('データの追加に成功しました<br>');
        }else{
            print('データの追加に失敗しました<br>');
        }
    }
?>
