<?php
    // require_once 'DbManager.php';
    require_once dirname(__FILE__) . '/../../config/DbManager.php';
    $pdo = getDb();

    $param = $_GET["shop_id"];

    date_default_timezone_set('Asia/Tokyo');
    
    if(isset($param)){
        // $sql = "SELECT shop_id, sum(left2right), sum(right2left) FROM dev_shop_status where shop_id = :shop AND id = 26";
        $sql = "SELECT shop_id, left2right, right2left FROM dev_shop_status where shop_id = :shop ORDER BY created_at DESC LIMIT 1";
        $stmt = $pdo -> prepare($sql);
        $stmt -> bindParam(':shop', $param, PDO::PARAM_INT);
        $stmt -> execute();
        
        $shopData = array();
        while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
            $shopData[]=array(
            'shop_id' => $row['shop_id'],
            'shop_name' => 'Statup Hub Tokyo',
            'left2right' => $row['left2right'],
            'right2left' => $row['right2left'],
            'updated_at' => date('Y/m/d H:i:s')
            );
        }
    }else{
        $sql = "SELECT * FROM dev_shop_status";
        $stmt = $pdo -> prepare($sql);
        $stmt -> execute();

        $shopData = array();
        while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
            $shopData[]=array(
            'shop_id' => $row['shop_id'],
            'left2right' => $row['left2right'],
            'right2left' => $row['right2left']
            );
        }
    }
    // jsonとして出力
    header('Content-type: application/json');
    echo json_encode($shopData);

?>