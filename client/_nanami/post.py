#!/usr/bin/env python
#this is monodukuri code
#-*-coding:utf-8-*-
#python version 2.7.8

import requests,json
from datetime import datetime


class PostRequest(object):
    def __init__(self, shop_id, left2right, right2left, url):
        self.payload = payload = {'s_id':'','l2r':'','r2l':'', 'domain':'', 'time_stamp':''}
        self.shop_id = shop_id
        self.left2right = left2right
        self.right2left = right2left
        self.url = url

    def value_attach(self):
        self.payload['s_id'] = self.shop_id
        self.payload['l2r'] = self.left2right
        self.payload['r2l'] = self.right2left
        self.payload['domain'] = self.url

    def send_request(self):
        try:
            self.payload['time_stamp'] = str(datetime.now())
            print url
            post_request = requests.post(url,json=self.payload)
            print "succesfully posted"
            print post_request.status_code
            post_request.json()
        except:
            print "error while posting data"
            print self.payload


if __name__=='__main__':
    # Sample usage
    shop_id = 1
    left2right = 1
    right2left = 1
    url = 'http://127.0.0.1:5000/post'

    # Post data
    data = PostRequest(shop_id, left2right, right2left ,url)
    data.value_attach()
    data.send_request()

    # For Test do  "pip install httpbin" & "python -m httpbin.core"
