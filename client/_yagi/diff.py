# refer to: http://qiita.com/hitomatagi/items/f9d4d6b816d729132231

import cv2
import numpy as np

# 定数定義
ESC_KEY = 27     # Escキー
INTERVAL= 33     # インターバル
FRAME_RATE = 30  # fps

WINDOW_ORG = "org"
WINDOW_BACK = "back"
WINDOW_DIFF = "diff"

FILE_ORG = "org_768x576.avi"

# preference
# THRESHOLD = 127
THRESHOLD = 40
MAXVALUE  = 255

# ウィンドウの準備
cv2.namedWindow(WINDOW_ORG)
cv2.namedWindow(WINDOW_BACK)
cv2.namedWindow(WINDOW_DIFF)

# 元ビデオファイル読み込み
# mov_org = cv2.VideoCapture(FILE_ORG)
mov_org = cv2.VideoCapture(0)
kernel = np.ones((5, 5), np.uint8)

# 最初のフレーム読み込み
has_next, i_frame = mov_org.read()

# 背景フレーム
back_frame = np.zeros_like(i_frame, np.float32)

# 変換処理ループ
while has_next == True:
    # 入力画像を浮動小数点型に変換
    f_frame = i_frame.astype(np.float32)

    # 差分計算
    diff_frame = cv2.absdiff(f_frame, back_frame)

    # 背景の更新
    # cv2.accumulateWeighted(f_frame, back_frame, 0.025)
    cv2.accumulateWeighted(f_frame, back_frame, 0.5)


    output = diff_frame.astype(np.uint8)
    _, bin_cv2 = cv2.threshold(output, THRESHOLD, MAXVALUE, cv2.THRESH_BINARY)
    # 膨張
    dilation = cv2.dilate(bin_cv2, kernel, iterations=8)

    gray = cv2.cvtColor(dilation, cv2.COLOR_RGB2GRAY)

    # 輪郭検出
    # image, contours, hierarchy = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    # print(contours)
    # cv2.drawContours(gray, contours, -1, (0,255,0), 3)
    # gray = cv2.drawContours(gray, contours, -1, (0,255,0), 3)


    # フレーム表示
    # cv2.imshow(WINDOW_ORG, i_frame)
    # cv2.imshow(WINDOW_BACK, back_frame.astype(np.uint8))
    # cv2.imshow(WINDOW_DIFF, diff_frame.astype(np.uint8))
    # cv2.imshow(WINDOW_DIFF, output)
    cv2.imshow(WINDOW_DIFF, gray)

    # Escキーで終了
    key = cv2.waitKey(INTERVAL)
    if key == ESC_KEY:
        break

    # 次のフレーム読み込み
    has_next, i_frame = mov_org.read()

# 終了処理
cv2.destroyAllWindows()
mov_org.release()