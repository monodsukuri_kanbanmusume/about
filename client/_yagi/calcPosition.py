# Output
# [ [x,y,m], [x,y,m], [x,y,m] ]

def calcPosition(x, y, w, h, m):
    _data = []
    _data.append(x + w/2)
    _data.append(y + h/2)
    _data.append(m)
    return _data


data = []

data.append(calcPosition(10, 20, 60, 80, 150))

print(data)