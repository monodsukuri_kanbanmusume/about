import io
import cv2
import sys
import time
import picamera
import picamera.array

faceCascade = cv2.CascadeClassifier('/home/pi/work/camera/opencv-3.1.0/data/haarcascades/haarcascade_frontalface_default.xml')
upperBodyCascade = cv2.CascadeClassifier('/home/pi/work/camera/opencv-3.1.0/data/haarcascades/haarcascade_upperbody.xml')
CAMERA_WIDTH  = 416
CAMERA_HEIGHT = 304

# Saving the picture to an in-program steam rather than a file
stream = io.BytesIO()

with picamera.PiCamera() as camera:
    camera.resolution = (CAMERA_WIDTH, CAMERA_HEIGHT)
    #camera.vflip = True
    camera.framerate = 60
    time.sleep(2)
    while(True):
        try:
            with picamera.array.PiRGBArray(camera) as stream:
                camera.capture(stream, format='bgr')
                # At this point the image is available as stream.array
                image = stream.array
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                faces = faceCascade.detectMultiScale(
                        	gray,
                        	scaleFactor=1.1,
                        	minNeighbors=5,
                        	minSize=(30, 30),
                        	flags=cv2.cv.CV_HAAR_SCALE_IMAGE
                        )
                upperBodys =  upperBodyCascade.detectMultiScale(
                        	gray,
                        	scaleFactor=1.1,
                        	minNeighbors=5,
                        	minSize=(30, 30),
                        	flags=cv2.cv.CV_HAAR_SCALE_IMAGE
                        )

                for (x, y, w, h) in faces:
                    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 0, 255), 2)
                for (x, y, w, h) in upperBodys:
                    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

                cv2.imshow('Test Image', image)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    cv2.destroyAllWindows()
                    exit
        except (KeyboardInterrupt):
            picamera.PiCamera().close()
